(function ($) {
// START jQuery

Drupal.vfr = Drupal.vfr || {};

Drupal.behaviors.vfr = function(context) {
  $('form#'+Drupal.settings.vfr.form_id+' input#edit-reset', context).click(function() {
    if (Drupal.settings.vfr.url) {
      window.location = Drupal.settings.vfr.url;
    }
    else {
      $('form#'+Drupal.settings.vfr.form_id, context).clearForm();
      $('form#'+Drupal.settings.vfr.form_id, context).submit();
    }
  });
}

// END jQuery
})(jQuery);

