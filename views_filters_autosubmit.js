(function ($) {
// START jQuery

Drupal.vfas = Drupal.vfas || {};

Drupal.behaviors.vfas = function(context) {
  $("form#"+Drupal.settings.vfas.form_id+" #"+Drupal.settings.vfas.submit_id, context).hide();
  $("form#"+Drupal.settings.vfas.form_id+" div.views-exposed-widget input:not("+Drupal.settings.vfas.exceptions+")", context).change(function() {
    $("form#"+Drupal.settings.vfas.form_id, context).submit();
  });
  $("form#"+Drupal.settings.vfas.form_id+" div.views-exposed-widget select:not("+Drupal.settings.vfas.exceptions+")", context).change(function() {
    $("form#"+Drupal.settings.vfas.form_id, context).submit();
  });
}

// END jQuery
})(jQuery);

